### Keybase proof

I hereby claim:

  * I am woodruffw on github.
  * I am yossarian (https://keybase.io/yossarian) on keybase.
  * I have a public key whose fingerprint is 46C3 9716 A8F0 7E98 384E  28F7 85AE 00C5 0483 3B3C

To claim this, I am signing this object:

```json
{
    "body": {
        "client": {
            "name": "keybase.io node.js client",
            "version": "0.7.8"
        },
        "key": {
            "fingerprint": "46c39716a8f07e98384e28f785ae00c504833b3c",
            "host": "keybase.io",
            "key_id": "85AE00C504833B3C",
            "uid": "1cc3e03391344775146125d33b6e4d19",
            "username": "yossarian"
        },
        "service": {
            "name": "github",
            "username": "woodruffw"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1432659828,
    "expire_in": 157680000,
    "prev": null,
    "seqno": 1,
    "tag": "signature"
}
```

with the key [46C3 9716 A8F0 7E98 384E  28F7 85AE 00C5 0483 3B3C](https://keybase.io/yossarian), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: GnuPG v1

owGbwMvMwMTYuo7hKEuztQ3j6QPvkhhCU5aXVCsl5adUKllVKyXnZKbmlYBYeYm5
qUpWStmplUmJxal6mfkKefkpqXpZxQpQNTpKZalFxZn5eUBVBnrmehZKtTog5SDN
aZl56alFBUWZILOUTMySjS3NDc0SLdIMzFMtLYwtTFKNLNLMLUwTUw0Mkk0NTCyM
jZOMk4FGZuQXl6DYqgQ2Mz4zBShqYeroamDgDFHvZOwMlCsFSxgmJxunGhgbWxoa
m5iYm5sampgZGpmmAA01SzVJMbQEKSxOLYJ6qTK/uDixKDMxD+RgoHBZZnIqko/T
M0sySpNQtZTn56cUlaallYO0lFQWgMVSk+KhuuOTMvNSgF5GDhNDoMrkkkyQdkMT
YyMzU0sLIwsdpdSKgsyi1PhMkApTczMLAyDQUSooSi1TssorzckBuagwLx8oC7Qo
MR1oT3Fmel5iSWlRqlJtJ5MMCwMjEwMbKxMo2hi4OAVgkWk4XYChY1rGLs0tE9J/
Cal6t1ap3mCaOPvGwn+cKQ85iwKzNe1i3n2ovCfTLsHwlbH0garlhaoPLYuX/Dx2
+hOnk4rit/MC7JOZA5hqzgdnfordbD7t0Zd/+xuiNO0PSUkIctwQfBGrYem1QaR4
+l7j949lWh0mTPvFNHtaqln3x7aJxQJ2Bfs+6m/6u+WqzKuDX945eUednKuWfLeu
cMOE1ezGy4KM5/599vXmUYuys2sVRBwOxEsJrX3258K73SKibrp6u5kaxGbs+PVN
ZqlOdq+dXU5D4/mOOeduPtp9WVpT1nrKzT5GHoWe1RdKD3LtOy/5ybHw/q5ync1O
5y+FG+cUbjXiS5l7I/XRnFk/5UpMXcSX3N+7Qogp+93WD9evXjoR19butvZyWIoY
d2zDacPpT/9bn067/WtK1Iyk9Ae7/qtke/rslYsT1TiZEdwaW/++JmKxBTBtNxze
VVg3cZGvluuFA5HdEzQ41LqqYzNvdwUmyz+ffM10RfiJdElZ55dt/9/emXjwguDS
VQeeik/7dfDV193fffZ/T/y7ZupxA/6rz9k/+HUeKznW/rPg9a1V+5eK1v5Ujszn
OcVsZinXeuI9X0z1zM5bradu3Xm42rt8wbKc+ffYVvX5zVl0p2Zuz3GbuMN370kp
MGXo8Vrnz1XTc/b88aIp5OvaTx6RV1fzetueCc7q7HjQ9ykkaPKFhZ8W+dQ4hvfO
XfThEwA=
=Gw23
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/yossarian

### From the command line:

Consider the [keybase command line program](https://keybase.io/docs/command_line).

```bash
# look me up
keybase id yossarian

# encrypt a message to me
keybase encrypt yossarian -m 'a secret message...'

# ...and more...
```
